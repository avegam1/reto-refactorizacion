package co.com.sofka.crud.domain.repository;

import co.com.sofka.crud.persistence.entity.Todo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoRepository extends CrudRepository<Todo, Long> {
}
