package co.com.sofka.crud.domain.repository;

import co.com.sofka.crud.persistence.entity.GroupList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupListRepository extends CrudRepository<GroupList,Long> {
}
